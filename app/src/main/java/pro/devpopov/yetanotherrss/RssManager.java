package pro.devpopov.yetanotherrss;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by sergey on 06.06.16.
 */
public class RssManager {
    private Context mContext;
    private static RssManager instance;

    private List<RssModel> rssData = Collections.synchronizedList(new ArrayList<RssModel>());
    private int currentModel = 0;

    public static RssManager getInstance() {
        return instance;
    }

    public static void initInstance(Context context) {
        if(instance == null) instance = new RssManager(context);
    }

    public RssManager(Context context) {
        mContext = context;
    }

    public void updateRssData() {
        SharedPreferences preferences = mContext.getSharedPreferences(mContext.getPackageName(), Context.MODE_PRIVATE);
        final String url = preferences.getString("rss", null);

        if (url != null) {
            new Thread() {
                public void run() {
                    try {
                        URL feed = new URL(url);
                        HttpURLConnection conn = (HttpURLConnection) feed.openConnection();

                        if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                            InputStream inputStream = conn.getInputStream();

                            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                                    .newInstance();
                            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

                            Document document = documentBuilder.parse(inputStream);
                            Element element = document.getDocumentElement();

                            NodeList nodeList = element.getElementsByTagName("item");

                            rssData = Collections.synchronizedList(new ArrayList<RssModel>());

                            if (nodeList.getLength() > 0) {
                                for (int nodeListIterator = 0; nodeListIterator < nodeList.getLength(); nodeListIterator++) {

                                    Element entry = (Element) nodeList.item(nodeListIterator);

                                    Element titleElement = (Element) entry.getElementsByTagName(
                                            "title").item(0);
                                    Element descriptionElement = (Element) entry
                                            .getElementsByTagName("description").item(0);

                                    String title = titleElement.getFirstChild().getNodeValue();
                                    String description = descriptionElement.getFirstChild().getNodeValue();

                                    RssModel rssModel = new RssModel(title, description);
                                    rssData.add(rssModel);
                                }
                            }

                        }
                    } catch (IOException | ParserConfigurationException | SAXException e) {
                        rssData = Collections.synchronizedList(new ArrayList<RssModel>());
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    }

    public RssModel getNext() {
        try {
            if (currentModel + 1 < rssData.size()) {
                return rssData.get(++currentModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public RssModel getPrevious() {
        try {
            if (currentModel - 1 >= 0) {
                return rssData.get(--currentModel);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public RssModel getCurrent() {
        try {
            if (currentModel >= 0 && currentModel < rssData.size()) {
                return rssData.get(currentModel);
            } else if (rssData.size() != 0) {
                currentModel = 0;
                return rssData.get(currentModel);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
