package pro.devpopov.yetanotherrss;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

import pro.devpopov.yetanotherrss.tools.validation.IValidator;
import pro.devpopov.yetanotherrss.tools.validation.NonEmptyValidator;
import pro.devpopov.yetanotherrss.tools.validation.LinkValidator;
import pro.devpopov.yetanotherrss.tools.validation.Validator;

/**
 * Created by sergey on 06.06.16.
 */
public class RssConfigureActivity extends AppCompatActivity {
    private int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rss_configure_layout);

        final EditText fieldInput = (EditText) findViewById(R.id.rss_field_input);

        final SharedPreferences preferences = getSharedPreferences(getApplicationInfo().packageName, Context.MODE_PRIVATE);

        final Validator validator = new Validator(new ArrayList<IValidator>() {{
            add(new NonEmptyValidator());
            add(new LinkValidator());
        }}, (TextInputLayout) findViewById(R.id.rss_field));

        String savedRss = preferences.getString("rss", "https://istria.hh.ru/search/vacancy/rss?area=232&order_by=salary_desc&search_period=3&specialization=1.221");
        fieldInput.setText(savedRss);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if(extras != null){
            mAppWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        if(mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID){
            finish();
        }

        findViewById(R.id.ok_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validator.validate())
                {
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("rss", fieldInput.getText().toString());
                    editor.putBoolean("configured", true);
                    editor.apply();

                    AlarmManager am=(AlarmManager)getSystemService(Context.ALARM_SERVICE);
                    Intent intent = new Intent(getApplicationContext(), RssBroadcastReceiver.class);
                    PendingIntent pi = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);

                    am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000, 1000, pi);

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
            }
        });
    }
}
