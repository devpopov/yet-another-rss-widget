package pro.devpopov.yetanotherrss;

import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.text.Html;
import android.widget.RemoteViews;

/**
 * Created by sergey on 06.06.16.
 */
public class RssBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            RssManager rssManager = RssManager.getInstance();
            rssManager.updateRssData();

            PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "YET ANOTHER RSS WIDGET");
            wakeLock.acquire();

            RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                    R.layout.rss_widget_layout);

            RssModel model = rssManager.getCurrent();

            if (model != null) {
                remoteViews.setTextViewText(R.id.title, model.getTitle());
                remoteViews.setTextViewText(R.id.description, Html.fromHtml(model.getDescription()));
            }
            else {
                remoteViews.setTextViewText(R.id.title, context.getResources().getString(R.string.error_no_info));
                remoteViews.setTextViewText(R.id.description, "");
            }
            ComponentName widget = new ComponentName(context, RssWidgetProvider.class);
            AppWidgetManager manager = AppWidgetManager.getInstance(context);
            manager.updateAppWidget(widget, remoteViews);

            wakeLock.release();
        }
}
