package pro.devpopov.yetanotherrss.tools.validation;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.util.ArrayList;

/**
 * Created by sergey on 06.06.16.
 */
public class Validator {
    private ArrayList<IValidator> mValidators;
    private TextInputLayout mTextLayout;

    public Validator(ArrayList<IValidator> validators, TextInputLayout textLayout) {
        mValidators = validators;
        mTextLayout = textLayout;

        mTextLayout.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mTextLayout.setError(null);
                mTextLayout.setErrorEnabled(false);
                EditText editText = mTextLayout.getEditText();
                editText.setSelection(editText.getText().length());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public boolean validate() {
        for(IValidator validator: mValidators) {
            if (!validator.validate(mTextLayout)) {
                return false;
            }
        }

        return true;
    }
}
