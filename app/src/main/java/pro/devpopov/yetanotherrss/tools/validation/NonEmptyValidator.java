package pro.devpopov.yetanotherrss.tools.validation;

import android.support.design.widget.TextInputLayout;

import pro.devpopov.yetanotherrss.R;

/**
 * Created by sergey on 06.06.16.
 */
public class NonEmptyValidator implements IValidator {
    @Override
    public boolean validate(TextInputLayout textLayout) {
        if(textLayout.getEditText().getText().length() == 0) {
            textLayout.setError(textLayout.getResources().getString(R.string.error_empty_field));
            return false;
        }
        return true;
    }
}
