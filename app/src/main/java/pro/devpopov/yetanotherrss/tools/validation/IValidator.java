package pro.devpopov.yetanotherrss.tools.validation;

import android.support.design.widget.TextInputLayout;

/**
 * Created by sergey on 06.06.16.
 */
public interface IValidator {
    boolean validate(TextInputLayout textLayout);
}
