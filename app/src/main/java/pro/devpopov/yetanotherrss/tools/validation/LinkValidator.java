package pro.devpopov.yetanotherrss.tools.validation;

import android.support.design.widget.TextInputLayout;
import android.webkit.URLUtil;

import pro.devpopov.yetanotherrss.R;

/**
 * Created by sergey on 06.06.16.
 */
public class LinkValidator implements IValidator {
    @Override
    public boolean validate(TextInputLayout textLayout) {
        String url = textLayout.getEditText().getText().toString();
        if(!URLUtil.isValidUrl(url)) {
            textLayout.setError(textLayout.getResources().getString(R.string.error_invalid_link));
            return false;
        }

        return true;
    }
}
