package pro.devpopov.yetanotherrss;

import android.app.Application;

/**
 * Created by sergey on 06.06.16.
 */
public class RssApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        RssManager.initInstance(getApplicationContext());
    }
}
