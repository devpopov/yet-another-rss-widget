package pro.devpopov.yetanotherrss;

/**
 * Created by sergey on 06.06.16.
 */
public class RssModel {
    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public RssModel(String title, String description) {
        mTitle = title;
        mDescription = description;
    }

    private String mTitle;
    private String mDescription;
}
