package pro.devpopov.yetanotherrss;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Html;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

public class RssWidgetProvider extends AppWidgetProvider {
    public static String ACTION_LEFT_CLICKED = "ActionLeftClicked";
    public static String ACTION_RIGHT_CLICKED = "ActionRightClicked";

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
    }

    @Override
    public void onDisabled(Context context) {
        Intent intent = new Intent(context, RssBroadcastReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);

        SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("configured", false);
        editor.apply();

        super.onDisabled(context);
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);

        SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        if (preferences.contains("configured")) {
            if (preferences.getBoolean("configured", false))
            {
                AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
                Intent intent = new Intent(context, RssBroadcastReceiver.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

                am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000, 1000, pendingIntent);
            }
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.rss_widget_layout);

        Intent active = new Intent(context, RssWidgetProvider.class);
        active.setAction(ACTION_LEFT_CLICKED);
        PendingIntent actionPendingIntent = PendingIntent.getBroadcast(context, 0, active, 0);
        remoteViews.setOnClickPendingIntent(R.id.left_button, actionPendingIntent);

        active = new Intent(context, RssWidgetProvider.class);
        active.setAction(ACTION_RIGHT_CLICKED);
        actionPendingIntent = PendingIntent.getBroadcast(context, 0, active, 0);
        remoteViews.setOnClickPendingIntent(R.id.right_button, actionPendingIntent);

        appWidgetManager.updateAppWidget(appWidgetIds, remoteViews);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (action.equals(ACTION_LEFT_CLICKED) || action.equals(ACTION_RIGHT_CLICKED)) {
            doNavigationAction(context, action);
        } else {
            super.onReceive(context, intent);
        }
    }

    public void doNavigationAction(Context context, String action) {
        RssManager rssManager = RssManager.getInstance();
        RssModel model = null;

        if (action.equals(ACTION_LEFT_CLICKED)) {
            model = rssManager.getPrevious();
        }
        else if (action.equals(ACTION_RIGHT_CLICKED)) {
            model = rssManager.getNext();
        }

        RemoteViews remoteView = new RemoteViews(context.getPackageName(), R.layout.rss_widget_layout);
        if (model != null) {
            remoteView.setTextViewText(R.id.title, model.getTitle());
            remoteView.setTextViewText(R.id.description, Html.fromHtml(model.getDescription()));
        }

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        ComponentName componentName = new ComponentName(context, RssWidgetProvider.class);

        appWidgetManager.updateAppWidget(componentName, remoteView);
    }
}
