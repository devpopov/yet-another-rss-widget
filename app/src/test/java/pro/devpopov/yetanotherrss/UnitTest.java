package pro.devpopov.yetanotherrss;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class UnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        URL feed = new URL("http://rss.nytimes.com/services/xml/rss/nyt/Africa.xml");
        HttpURLConnection conn = (HttpURLConnection) feed.openConnection();

        if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
            InputStream is = conn.getInputStream();

            DocumentBuilderFactory dbf = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();

            Document document = db.parse(is);
            Element element = document.getDocumentElement();

            NodeList nodeList = element.getElementsByTagName("item");

            if (nodeList.getLength() > 0) {
                for (int i = 0; i < nodeList.getLength(); i++) {

                    Element entry = (Element) nodeList.item(i);

                    Element _titleE = (Element) entry.getElementsByTagName(
                            "title").item(0);
                    Element _descriptionE = (Element) entry
                            .getElementsByTagName("description").item(0);

                    String _title = _titleE.getFirstChild().getNodeValue();
                    String _description = _descriptionE.getFirstChild().getNodeValue();

                    RssModel rssModel = new RssModel(_title, _description);
                }
            }
        }
    }
}